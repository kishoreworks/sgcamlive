package com.kishore.sglivecam

import android.app.Application
import dagger.hilt.android.HiltAndroidApp
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@HiltAndroidApp
class SGLiveCamApplication : Application(){

    override fun onCreate() {
        super.onCreate()
    }
}
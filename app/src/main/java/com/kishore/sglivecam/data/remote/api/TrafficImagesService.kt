package com.kishore.sglivecam.data.remote.api

import com.kishore.sglivecam.model.SGTraffic
import retrofit2.Response
import retrofit2.http.GET
/**
 * Service to fetch Cmera Images using end point [BASE_URL].
 */
interface TrafficImagesService {

    @GET("transport/traffic-images")
    suspend fun getTrafficImages(): Response<SGTraffic>

    companion object {
        const val BASE_URL = "https://api.data.gov.sg/v1/"
    }
}


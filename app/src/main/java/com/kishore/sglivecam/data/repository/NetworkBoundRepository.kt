package com.kishore.sglivecam.data.repository

import androidx.annotation.MainThread
import com.kishore.sglivecam.model.State
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.*
import retrofit2.Response
/**
 * A repository which provides resource from remote end point.
 */
@ExperimentalCoroutinesApi
abstract class NetworkBoundRepository<T> {

    fun asFlow() = flow<State<T>> {

        // Emit Loading State
        emit(State.loading())

        // Fetch latest posts from remote
        val apiResponse = fetchFromRemote()

        // Parse body
        val remotePosts = apiResponse.body()

        // Check for response validation
        if (apiResponse.isSuccessful && remotePosts != null) {
                emit(
                    State.success(remotePosts)
                )
        } else {
            // Something went wrong! Emit Error state.
            emit(State.error(apiResponse.message()))
        }
    }.catch { e ->
        // Exception occurred! Emit error
        emit(State.error("Network error! Can't get latest posts."))
        e.printStackTrace()
    }
    /**
     * Fetches [T] from the remote end point.
     */
    @MainThread
    protected abstract suspend fun fetchFromRemote(): Response<T>
}

package com.kishore.sglivecam.data.repository

import com.kishore.sglivecam.data.remote.api.TrafficImagesService
import com.kishore.sglivecam.model.SGTraffic
import com.kishore.sglivecam.model.State
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import retrofit2.Response
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Singleton repository for fetching data from remote
 */
@ExperimentalCoroutinesApi
@Singleton
class SgCamRepository @Inject constructor(private val trafficImagesService: TrafficImagesService){

    /**
     * Fetched the Traffic coordinates from network and emitted.
     */
     fun getTrafficCam(): Flow<State<SGTraffic>>{
         return object : NetworkBoundRepository<SGTraffic>() {
             override suspend fun fetchFromRemote(): Response<SGTraffic> = trafficImagesService.getTrafficImages()
         }.asFlow()
    }
}
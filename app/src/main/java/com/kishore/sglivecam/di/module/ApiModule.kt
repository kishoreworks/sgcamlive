package com.kishore.sglivecam.di.module

import com.kishore.sglivecam.data.remote.api.TrafficImagesService
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Singleton


@InstallIn(ApplicationComponent::class)
@Module
class ApiModule {
    @Singleton
    @Provides
    fun provideRetrofitService(): TrafficImagesService = Retrofit.Builder()
        .baseUrl(TrafficImagesService.BASE_URL)
        .addConverterFactory(
            MoshiConverterFactory.create(
                Moshi.Builder().add(KotlinJsonAdapterFactory()).build()
            )
        )
        .build()
        .create(TrafficImagesService::class.java)
}


package com.kishore.sglivecam.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class SGTraffic(
    var items: List<CamItems>?,
    @Json(name = "api_info")
    var apiInfo: ApiInfo?
){
    data class ApiInfo(
        var status: String?
    )

    data class CamItems(
        var timestamp: String?,
        var cameras: List<Camera>?
    )

    data class Camera(
        var timestamp: String? = null,
        var image: String? = null,
        var location: Location? = null,
        @Json(name="camera_id")
        var cameraId: String? = null
    )
    data class Location(
        var latitude: String? = null,
        var longitude: String? = null
    )
}
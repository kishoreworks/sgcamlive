package com.kishore.sglivecam.ui.map

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.lifecycle.observe
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.kishore.sglivecam.R
import com.kishore.sglivecam.databinding.ActivityMapBinding
import com.kishore.sglivecam.model.SGTraffic
import com.kishore.sglivecam.model.State
import com.kishore.sglivecam.ui.base.BaseActivity
import com.kishore.sglivecam.ui.map.adapter.CustomInfoWindowAdapter
import com.kishore.sglivecam.utils.showToast
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@ExperimentalCoroutinesApi
@AndroidEntryPoint
class MapActivity : BaseActivity<MapViewModel, ActivityMapBinding>() {

    private lateinit var googleMap: GoogleMap
    private var mapReady = false
    private var cameras: List<SGTraffic.Camera>? = null
    private var markerAdapter: CustomInfoWindowAdapter? = null
    private var hasCameraRefresh: Boolean = true


    override val mViewModel: MapViewModel by viewModels()
    override fun getViewBinding(): ActivityMapBinding = ActivityMapBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mViewBinding.root)
        initData()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_map, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_refresh -> {
                hasCameraRefresh = false
                getCameras()}
        }
        return true;
    }

    /**
     * Init the components and Observe data
     */
    private fun initData() {
        mViewModel.camerasLiveData.observe(this) { state ->
            when (state) {
                is State.Loading -> {showLoading(true)}
                is State.Success -> {
                    state.data.let {
                        showLoading(false)
                        cameras = state.data.items?.getOrNull(0)?.cameras ?: emptyList()
                        updateMap()
                    }
                }
                is State.Error -> {
                    showToast(state.message)
                    showLoading(false)
                }
            }
        }

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment
        mapFragment?.getMapAsync { googleMap ->
            this.googleMap = googleMap
            mapReady = true
            updateMap()
        }

        // If State isn't `Success` then reload posts.
        if (mViewModel.camerasLiveData.value !is State.Success) {
            getCameras()
        }
        markerAdapter = CustomInfoWindowAdapter(this)
    }

    /**
     * If State isn't `Success` then reload posts.
     */
    private fun showLoading(isLoading: Boolean) {
        mViewBinding.progressBar.visibility = if (isLoading) View.VISIBLE else View.INVISIBLE
    }

    private fun getCameras() {
        mViewModel.getCameras()
    }

    /**
     * Update marker when the map is ready
     */
    private fun updateMap() {
        if (mapReady && cameras != null) {
            googleMap.clear()
            googleMap.setInfoWindowAdapter(markerAdapter)
            cameras?.forEachIndexed { index, camera ->
                val latitude = camera.location?.latitude
                val longitude = camera.location?.longitude
                if (latitude?.isEmpty()?.not() == true && longitude?.isEmpty()?.not() == true) {
                    val coordinate = LatLng(latitude.toDouble(), longitude.toDouble())
                    googleMap.addMarker(MarkerOptions().position(coordinate).title(camera.image))

                    if (index == 0 && hasCameraRefresh) {
                        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(coordinate, 11F))
                    }
                }
            }
        }
    }
}
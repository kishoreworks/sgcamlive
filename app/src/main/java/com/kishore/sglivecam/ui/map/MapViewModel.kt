package com.kishore.sglivecam.ui.map

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.kishore.sglivecam.data.repository.SgCamRepository
import com.kishore.sglivecam.model.SGTraffic
import com.kishore.sglivecam.model.State
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

/**
 * ViewModel for [MapActivity]
 */
@ExperimentalCoroutinesApi
class MapViewModel @ViewModelInject constructor(private val sgCamRepository: SgCamRepository):ViewModel() {

    private val _camerasLiveData = MutableLiveData<State<SGTraffic>>()

    val camerasLiveData: LiveData<State<SGTraffic>>
        get() = _camerasLiveData

    fun getCameras() {
        viewModelScope.launch {
            sgCamRepository.getTrafficCam().collect {
                _camerasLiveData.value = it
            }
        }
    }

}
package com.kishore.sglivecam.ui.map.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.kishore.sglivecam.R
import com.kishore.sglivecam.databinding.ViewMapInfoviewBinding
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso


class CustomInfoWindowAdapter(private val context: Context) : GoogleMap.InfoWindowAdapter{
    private val infoView = ViewMapInfoviewBinding.inflate(LayoutInflater.from(context))

    override fun getInfoWindow(marker: Marker?): View? = null

    override fun getInfoContents(marker: Marker?): View?  {
        render(marker)
        return infoView.root
    }
    private fun render(marker: Marker?) {
        Picasso
            .get()
            .load(marker?.title)
            .error(R.drawable.traffic_placeholder)
            .into(infoView.imgTraffic, MarkerCallback(marker, marker?.title, infoView.imgTraffic))
    }
}

class MarkerCallback(private val marker: Marker?,
                     private val url: String?,
                     private val imageView: ImageView): Callback {
    override fun onSuccess() {
        if (marker?.isInfoWindowShown == true) {
            marker?.hideInfoWindow()

            Picasso.get()
                .load(url)
                .placeholder(R.drawable.traffic_placeholder)
                .into(imageView)
            marker?.showInfoWindow()
        }
    }

    override fun onError(e: Exception?) {}
}
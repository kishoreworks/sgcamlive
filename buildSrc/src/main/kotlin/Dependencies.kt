object Dependencies {
    const val kotlin = "org.jetbrains.kotlin:kotlin-stdlib:1.4.20"
    const val kotlinGradle = "org.jetbrains.kotlin:kotlin-gradle-plugin:1.4.20"
    const val gradle = "com.android.tools.build:gradle:4.1.1"
    const val daggerHilt = "com.google.dagger:hilt-android-gradle-plugin:2.28-alpha"
    const val ktLint = "org.jlleitschuh.gradle:ktlint-gradle:9.2.1"
    const val googleServices = "com.google.gms:google-services:4.3.4"
}

object Coroutines {
    const val core = "org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.1"
    const val android = "org.jetbrains.kotlinx:kotlinx-coroutines-android:1.4.1"
}

object Design {
    const val imageLoading = "com.squareup.picasso:picasso:2.71828"
}

object Android {
    const val appcompat = "androidx.appcompat:appcompat:1.1.0"
    const val activityKtx = "androidx.activity:activity-ktx:1.1.0"
    const val coreKtx = "androidx.core:core-ktx:1.2.0"
    const val constraintLayout = "androidx.constraintlayout:constraintlayout:1.1.3"
    const val swipeRefreshLayout = "androidx.swiperefreshlayout:swiperefreshlayout:1.0.0"
}

object Lifecycle {
    const val viewModel = "androidx.lifecycle:lifecycle-viewmodel-ktx:2.2.0"
    const val liveData = "androidx.lifecycle:lifecycle-livedata-ktx:2.2.0"
}

object Google {
    const val map = "com.google.android.gms:play-services-maps:17.0.0"
}

object Retrofit {
    const val retrofit = "com.squareup.retrofit2:retrofit:2.8.1"
    const val moshiRetrofitConverter = "com.squareup.retrofit2:converter-moshi:2.7.2"
}

object Hilt {
    const val daggerCompiler = "com.google.dagger:hilt-android-compiler:2.28-alpha"
    const val hiltCompiler = "androidx.hilt:hilt-compiler:1.0.0-alpha01"
    const val hiltViewModel = "androidx.hilt:hilt-lifecycle-viewmodel:1.0.0-alpha01"
    const val hiltAndroid = "com.google.dagger:hilt-android:2.28-alpha"
}

object Moshi {
    const val moshi = "com.squareup.moshi:moshi-kotlin:1.9.2"
    const val codeGen = "com.squareup.moshi:moshi-kotlin-codegen:1.9.2"
}

object Testing {
    const val coroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-test:1.4.1"
    const val room = "androidx.room:room-testing:2.2.5"
    const val jUnit = "junit:junit:4.13"
    const val extJUnit = "androidx.test.ext:junit:1.1.1"
    const val espresso = "androidx.test.espresso:espresso-core:3.2.0"
    const val okHttp = "com.squareup.okhttp3:mockwebserver:4.9.0"
    const val core = "androidx.arch.core:core-testing:2.1.0"

}